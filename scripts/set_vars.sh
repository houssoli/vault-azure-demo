#!/bin/sh

# Check for existing of vault-demo.json file
[[ -z $CREDS_FILE_PATH ]] && CREDS_FILE_PATH=vault-demo.json
[[ ! -f $CREDS_FILE_PATH ]] && echo "ERROR: Could not find $CREDS_FILE_PATH. Please ensure this file is in the current directly, or explicitly set a CREDS_FILE_PATH environment variable"

# Check variables
export ARM_CLIENT_ID="$(cat ${CREDS_FILE_PATH} | jq -r .appId)"
export ARM_TENANT_ID="$(cat ${CREDS_FILE_PATH} | jq -r .tenant)"
export ARM_CLIENT_SECRET="$(cat ${CREDS_FILE_PATH} | jq -r .password)"

# Set default resource group name
[[ -z $AZ_SECRET_PATH ]] \
  && export AZ_SECRET_PATH="azure-demo" \
  && echo "Setting default AZ_SECRET_PATH variable to: $AZ_SECRET_PATH"

[[ -z $RESOURCE_GROUP ]] \
  && export RESOURCE_GROUP="rg-alice-demoapp-dev" \
  && echo "Setting default RESOURCE_GROUP variable to: $RESOURCE_GROUP"
