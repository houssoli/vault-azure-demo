#!/bin/bash

if [[ ! -z $ARM_SUBSCRIPTION_ID ]] \
       && [[ ! -z $ARM_CLIENT_ID ]] && [[ $ARM_CLIENT_ID != "null" ]] \
       && [[ ! -z $ARM_TENANT_ID ]] && [[ $ARM_TENANT_ID != "null" ]] \
       && [[ ! -z $ARM_CLIENT_SECRET ]] && [[ $ARM_CLIENT_SECRET != "null" ]] \
       && [[ ! -z $RESOURCE_GROUP ]] \
       && [[ ! -z $AZ_SECRET_PATH ]]
then
  echo "OK: All variables are set."
else
    echo "ERROR: Could not validate all variables. Please run source scripts/set_vars.sh or export the variables directly:"

    echo "Checking for ARM_SUBSCRIPTION_ID"
    [[ -z $ARM_SUBSCRIPTION_ID ]] \
        && echo "ERROR: ARM_SUBSCRIPTION_ID is not set. Please set it using export ARM_SUBSCRIPTION_ID=<id>" \
        || echo "OK"

    echo "Checking for ARM_CLIENT_ID"
    [[ -z $ARM_CLIENT_ID ]] || [[ $ARM_CLIENT_ID == "null" ]] \
        && echo "ERROR: ARM_CLIENT_ID is not set or is null" || echo "OK"

    echo "Checking for ARM_TENANT_ID"
    [[ -z $ARM_TENANT_ID ]] || [[ $ARM_TENANT_ID == "null" ]] \
        && echo "ERROR: ARM_TENANT_ID is not set or is null" || echo "OK"

    echo "Checking for ARM_CLIENT_SECRET"
    [[ -z $ARM_CLIENT_SECRET ]] || [[ $ARM_CLIENT_SECRET == "null" ]] \
        && echo "ERROR: ARM_CLIENT_SECRET is not set or is null" || echo "OK"

    echo "Checking for RESOURCE_GROUP"
    [[ -z $RESOURCE_GROUP ]] \
        && echo "ERROR: RESOURCE_GROUP is not set. Please run export RESOURCE_GROUP=rg-alice-hashicat-dev" || echo "OK"

    echo "Checking for AZ_SECRET_PATH"
    [[ -z $AZ_SECRET_PATH ]] \
        && echo "ERROR: AZ_SECRET_PATH is not set. Please run export AZ_SECRET_PATH=azure-demo" || echo "OK"

fi

