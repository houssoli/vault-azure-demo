#!/bin/sh
# This script checks whether required TFC variables are set
# If all variables are successful, it writes a workspace_id file
rm -f vars_are_valid

# Check if pre-req tools are installed
tools='["jq","vault"]'
for t in $(echo $tools | jq -r '.[]')
do
  if ! [ -x "$(command -v $t)" ]; then
    echo "Error: $t is not installed. This script relies on $t. Please install retry."
    exit 1
  fi
done


# Check if VAULT_TOKEN exists
if [ ! -z "$VAULT_TOKEN" ]; then
  echo "VAULT_TOKEN environment variable was found."
else
  echo "ERR: VAULT_TOKEN environment variable was not set."
  echo "You must export/set the VAULT_TOKEN environment variable."
  echo "It should be the root or an admin token"
  echo "Exiting."
  exit 1
fi

# Evaluate $VAULT_ADDR environment variable
# If not set, give error and exit
if [ ! -z "$VAULT_ADDR" ]; then
  echo "Using Vault Address: ${VAULT_ADDR}."
else
  echo "ERR: You must export/set the VAULT_ADDR environment variable."
  echo "Exiting."
  exit 1
fi

# Check if TFC_TOKEN exists
if [ ! -z "$TFC_TOKEN" ]; then
  echo "TFC_TOKEN environment variable was found."
else
  echo "ERR: TFC_TOKEN environment variable was not set."
  echo "You must export/set the TFC_TOKEN environment variable."
  echo "It should be a user or team token that has write or admin"
  echo "permission on the workspace."
  echo "Exiting."
  exit 1
fi

# Evaluate $CREDS_FILE_PATH environment variable
# If not set, give error and exit
if [ ! -z "$CREDS_FILE_PATH" ]; then
  echo "Using Main Azure secret credential in file: ${CREDS_FILE_PATH}."
else
  echo "ERR: You must export/set the CREDS_FILE_PATH environment variable."
  echo "Exiting."
  exit 1
fi

# Evaluate $AZ_SECRET_PATH environment variable
# If not set, give error and exit
if [ ! -z "$AZ_SECRET_PATH" ]; then
  echo "Using Vault Secret mount path: ${AZ_SECRET_PATH}."
else
  echo "ERR: You must export/set the AZ_SECRET_PATH environment variable."
  echo "Exiting."
  exit 1
fi

# Evaluate $RESOURCE_GROUP environment variable
# If not set, give error and exit
if [ ! -z "$RESOURCE_GROUP" ]; then
  echo "Using Resource group name: ${RESOURCE_GROUP}."
else
  echo "ERR: You must export/set the RESOURCE_GROUP environment variable."
  echo "Exiting."
  exit 1
fi

touch vars_are_valid
