#!/bin/sh

# Check for existing of vault-app.json file
[[ -z $CREDS_FILE_PATH ]] && CREDS_FILE_PATH=vault_app_creds.json
[[ ! -f $CREDS_FILE_PATH ]] && echo "ERROR: Could not find $CREDS_FILE_PATH. Please ensure this file is in the current directly, or\
 explicitly set a CREDS_FILE_PATH environment variable"; error=1

# Check variables
export ARM_CLIENT_ID="$(cat ${CREDS_FILE_PATH} | jq -r .data.client_id)"
export ARM_CLIENT_SECRET="$(cat ${CREDS_FILE_PATH} | jq -r .data.client_secret)"
